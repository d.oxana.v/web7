// Функция внутри $( document ).ready() срабатывает после загрузки DOM.
$(document).ready(function() {
  $(".slick").slick({
    dots: true,
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1250,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });
});